# :nodoc:
require 'microsoft_graph_auth'
require 'oauth2'
class ApplicationController < ActionController::Base
  # before_filter :set_gettext_locale
  # before_action :authenticate_user!
  protect_from_forgery unless: -> { request.format.json? }
  # load_and_authorize_resource
  before_action :set_user

  # before_action :logged_in_user, except: [:download]
  include ApplicationHelper
  include SessionsHelper

  rescue_from ActiveRecord::RecordNotFound, :with => :render_404 #Redirect jika parsing data gak ketemu

  rescue_from CanCan::AccessDenied do |exception|
    # redirect_to root_url, alert: exception.message
    if logged_in?
      if current_user.role_id <= 13 or current_user.role_id == 19
        redirect_to root_path, alert: exception.message
        # else
        # redirect_to incoming_letters_url, alert: exception.message
      end
    else
      redirect_to login_url
    end
  end

  def set_user
    @user_name = user_name
    @user_email = user_email
  end

  def save_in_session(auth_hash)
    session[:graph_token_hash] = auth_hash[:credentials]
    session[:user_name] = auth_hash.dig(:extra, :raw_info, :displayName)
    session[:user_email] = auth_hash.dig(:extra, :raw_info, :mail) || auth_hash.dig(:extra, :raw_info, :userPrincipalName)
    session[:user_timezone] = auth_hash.dig(:extra, :raw_info, :mailboxSettings, :timeZone)
    @username = session[:user_email].split('@')
    @user = Member.find_by(username: @username[0])
    if @user.present? 
      @getPermission = Permission.where(member_id: @user.member_id).first
      # session[:username] = @user.username
      # session[:role_id] = @getPermission.role_id
      # session[:permission_id] = @getPermission.permission_id
      saveSession(@getPermission.permission_id, auth_hash[:credentials])
    else
      user = Member.new
      user.username = @username[0]
      user.email = session[:user_email]
      user.password = "PGN50lut10n!"
      user.access_token = encrypt('PGN50lut10n!')
      user.status = "Active"
      @nama = @username[0].gsub(".", " ")
      @nama = @nama.upcase
      user.nama = @nama
      user.save
      @user = Member.find_by(username: @username[0])
      permission = Permission.new
      permission.member_id = @user.member_id
      permission.role_id = 10
      if permission.save
        @position = Permission.where(member_id: @user.member_id).first
        log_in @position
        permission = Permission.where(permission_id: session[:id]).first
        login_activity current_user.nama + " has been login "
        saveSession(@position.permission_id, auth_hash[:credentials])
      else
        redirect_to login_url, alert: "Username / Password anda salah"
      end
    end
  end

  def user_name
    session[:user_name]
  end

  def user_email
      session[:user_email]
  end

  def user_timezone
      session[:user_timezone]
  end

  def access_token
    token_hash = session[:graph_token_hash]

    expiry = Time.at(token_hash[:expires_at] - 300)

    if Time.now > expiry
      new_hash = refresh_tokens token_hash
      new_hash[:token]
    else
      token_hash[:token]
    end
  end
  
  def refresh_tokens(token_hash)
    oauth_strategy = OmniAuth::Strategies::MicrosoftGraphAuth.new(
      nil, ENV['AZURE_APP_ID'], ENV['AZURE_APP_SECRET']
    )

    token = OAuth2::AccessToken.new(
      oauth_strategy.client, token_hash[:token],
      :refresh_token => token_hash[:refresh_token]
    )

    new_tokens = token.refresh!.to_hash.slice(:access_token, :refresh_token, :expires_at)

    new_tokens[:token] = new_tokens.delete :access_token

    session[:graph_token_hash] = new_tokens
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  def token_exist?
    unless token?
      # flash[:danger] = "Please log in."
      reset_session
      redirect_to login_url, alert: "akun anda salah"
    end
  end

  # You want to get exceptions in development, but not in production.
  unless Rails.application.config.consider_all_requests_local
    rescue_from ActionController::RoutingError, with: -> { render_404 }
  end
  rescue_from AbstractController::ActionNotFound, with: -> { render_404 }

  def render_404
    respond_to do |format|
      format.html { render template: 'errors/error_404', status: 404 }
      format.all { render nothing: true, status: 404 }
    end
  end

  def custom_paginate_renderer
    # Return nice pagination for materialize
    Class.new(WillPaginate::ActionView::LinkRenderer) do
      def container_attributes
        {class: "pagination"}
      end

      def page_number(page)
        if page == current_page
          "<li class=\"cyan active\">"+link(page, page, rel: rel_value(page))+"</li>"
        else
          "<li class=\"waves-effect\">"+link(page, page, rel: rel_value(page))+"</li>"
        end
      end

      def previous_page
        num = @collection.current_page > 1 && @collection.current_page - 1
        previous_or_next_page(num, "<i class=\"material-icons\">Previous</i>")
      end

      def next_page
        num = @collection.current_page < total_pages && @collection.current_page + 1
        previous_or_next_page(num, "<i class=\"material-icons\">Next</i>")
      end

      def previous_or_next_page(page, text)
        if page
          "<li class=\"waves-effect\">"+link(text, page)+"</li>"
        end
      end
    end
  end
end
